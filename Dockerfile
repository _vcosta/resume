FROM alpine:3.11

ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true

RUN apk update && apk --no-cache add wkhtmltopdf nodejs npm \
    && rm -rf /tmp/* /var/tmp/* /var/cache/apk/* /var/lib/apk/*

RUN npm install -g \
    hackmyresume \
    resume-cli \
    jsonresume-theme-macchiato-ibic \
    jsonresume-theme-mocha-responsive \
    jsonresume-theme-kards \
    jsonresume-theme-kendall
